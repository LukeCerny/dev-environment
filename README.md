# DEV Environment

## Build

```bash
docker-compose build
```

## PHPStorm

### phpXYZ

1. **Add CLI Interpret** (Settings > PHP)
    - PHP language level: **XYZ**
    - CLI Interpret - **...**
        - Server: **Docker**
        - Configuration file: **./docker-compose.yml**
        - Service: **phpXYZ**
        - Always start a new container
    - Path mappings: **<Project root> -> /app**

2. **CodeSniffer** (Settings > PHP > Quality Tools)
    - Configuration - **...**
    - Left Panel - **+**
        - Interpret - phpXYZ
    - PHP_CodeSniffer path: **/dev-ops/bin/phpcs**

### node

1. Add plugin **Node.js remote interpret**
2. 